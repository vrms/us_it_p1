/*Erstellen Sie ein C-Programm, das die Ausgabe „Die Zahl Pi hat den Wert: 3.1415“ erzeugt. Benutzen Sie für die Ausgabe einen Parameter und ein %. Die Anführungszeichen sollen nicht ausgegeben werden.*/

#include <stdio.h>

int main()
{

	printf("Die Zahl hat den Wert %12.3f.\n", 3.1415); // %f
	printf("Die Zahl hat den Wert %f.", 3.1415); // %f
	
	// Datentyp muss Kommazahl sein, daher f = float
	// %.<anzahl_nachkommastellen>f --> steuert die Anzahl der Kommastellen

	return 0;
}
