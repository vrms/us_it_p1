#include <stdio.h>
#include <stdlib.h>

int main()

{
  system("clear");

  float kosten[12][6] = {
                          {45,56,78,124,234,78},
                          {45.90,57.12,79.56,126.48,238.68,79.56},
                          {46.82,58.26,81.15,129.01,243.45,81.15},
                          {47.75,59.43,82.77,131.59,248.32,82.77},
                          {48.71,60.62,84.43,134.22,253.29,84.43},
                          {49.68,61.83,86.12,136.91,258.35,86.12},
                          {50.68,63.07,87.84,139.64,263.52,87.84},
                          {51.69,64.33,89.60,142.44,268.79,89.60},
                          {52.72,65.61,91.39,145.29,274.17,91.39},
                          {53.78,66.93,93.22,148.19,279.65,93.22},
                          {54.85,68.26,95.08,151.16,285.24,95.08},
                          {55.95,69.63,96.98,154.18,290.95,96.98}
                        };

  printf("\n------ Zeilenweise -------------------------\n");
  printf("\n\t\t     dep1      dep2      dep3     dep4      dep5       dep6");
  for(int month=0; month<12; month++)
  {
    printf("\n month %d\t", month+1);
    for (int dep=0; dep<6; dep++)
    {
      printf("%10.2f",kosten[month][dep] );
    }
  }

  printf("\n----------------------------------------------\n");

  float totalSumme=0;
  for(int month=0; month<12; month++)
  {
    for (int dep=0; dep<6; dep++)
    {
      totalSumme = totalSumme+kosten[dep][month];
    }
  }

  printf("Gesamtsumme:\t EUR %8.2f", totalSumme);

  printf("\n\n--------Summe Abteilungen-------------------\n");

  for(int dep=0; dep<6; dep++)
  {
    float summe=0;
    for (int month=0; month<12; month++)
    summe = summe+kosten[dep][month];
    printf("\n Department %d", dep+1);
    printf("\t EUR %8.2f", summe);
  }

  printf("\n----------------------------------------------\n");




   return 0;
}
