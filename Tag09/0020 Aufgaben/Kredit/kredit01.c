#include <stdio.h>
#include <stdlib.h>

int main()

{
  float creditSum, interestRate, repayment;
  // int creditSum, interestRate, repayment;
  // system("clear");

  printf("\n please enter the total Credit sum: ");
  scanf("%f", &creditSum);
  printf("\n Please enter the interest rate: ");
  scanf("%f", &interestRate);
  printf("\n Please enter the monthly repayment amount: ");
  scanf("%f", &repayment);

  printf("\n\n %.2f | %.2f | %.2f\n\n", creditSum, interestRate, repayment);

  if (scanf("%.2f", &creditSum) != 1)
    { puts("INPUT ERROR 1"); exit(1);}
  if (scanf("%.2f", &interestRate) != 1)
    { puts("INPUT ERROR 2"); exit(1);}
  if (scanf("%.2f", &repayment) != 1)
    { puts("INPUT ERROR 3"); exit(1);}

   return 0;
}
