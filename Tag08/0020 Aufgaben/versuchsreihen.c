#include <stdio.h>
#include <stdlib.h>

int main()

{
  system("clear");


  int reihenNo=1, messwert=1;

  // loop for Versuchsreihen 1-10
  while (reihenNo<=10)
  {
    printf("\n  Versuchsreihe %2d\t---->\t|", reihenNo);

    // loop for Messwerte 1-5
    while (messwert<=5)
    {
      printf(" mw: %d | ", messwert);
      messwert++;  // benedet inneren horizontalen loop nach 5
    }
    messwert=1;   // laesst uns in l 18 wieder in den loop herein springen
    reihenNo++;
  }

  printf("\n\n");

   return 0;
}
