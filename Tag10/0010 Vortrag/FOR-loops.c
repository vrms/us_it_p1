#include <stdio.h>
#include <stdlib.h>

int main()

{

  system("clear");

  int i=2;

  for (i; i<88; i=i*i)
  {
      printf("\n i = %3d" , i);
  }

  printf("\n===============================================================\n");


  for (int j=49; j<55; j++)
  {
      printf("\nj = %d" , j);
  }

  printf("\n===============================================================\n");

  for (float m=4.239; m<29; m=m+3.086)
  {
  printf("\n m = %8.3f", m);
  }

  printf("\n===============================================================\n");


  for (float n=4.239; n<30; n=n*1.89)
  {
  printf("\n n = %6.2f", n);
  }

  printf("\n===============================================================\n");

  // im folgenden Loop soll der Bereich 15-25 uebersprungen werden
  for (float l=11; l<29; l=l+1)
  {
    if (l>15 && l<25)
    {
      continue;     // "continue" skips the below part of the loop & jumps back directly back to the head
    }
    printf("\n l = %6.1f", l);
  }

  printf("\n Der Loop wurde mit 'continue' unterbrochen");

  printf("\n===============================================================\n");

  // im folgenden Loop soll bei WErten ueber 15 abgebrochen werden
  for (float l=9; l<28; l=l+1)
  {
    if (l>=15)
    {
      break;     // "break" stops the continuation of the loop
    }
    printf("\n l = %6.1f", l);
  }

  printf("\n Der Loop wurde mit 'break' verlassen");

  printf("\n===============================================================\n");

  for (int x=1, y=5; x<20 && y<30; x++, y=y+2)
  {
    printf("\n x ist %2d und y ist %2d", x, y);
  }

  printf("\n===============================================================\n");

  int q=10;                     // works but is a bad idea
  for( ; q<20 ; q++)            // works but is a bad idea
  {
    printf("\n q ist %d ", q);
    q++;                        // works but is a bad idea
  }

  printf("\n===============================================================\n");

   return 0;
}
