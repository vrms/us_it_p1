#include <stdio.h>
#include <stdlib.h>

int main()

{
  system("clear");


  // defining some Variables

  float anzahlNaechte, preisProNacht, netTotal, grossTotal;
  float vat = 0.07;

  // printf("\n----------------------------------------------\n");

  printf("\nBitte Anzahl der Nächte eingeben: ");
  scanf("%f", &anzahlNaechte );
  printf("\nBitte Zimmerpreis pro Nacht eingeben: ");
  scanf("%f", &preisProNacht );

  netTotal = anzahlNaechte * preisProNacht;

  if(anzahlNaechte > 10)
  {
    netTotal = netTotal * 0.9;
  }
  if(anzahlNaechte >= 5 && anzahlNaechte < 10)
  {
    netTotal = netTotal * 0.97;
  }
  else
    netTotal = netTotal;

  float MwSt = netTotal * vat;
  grossTotal = netTotal * (1+vat);

  printf("\n %.0f Naechte\n", anzahlNaechte);
  printf("\n Netto Preis pro Nacht: %.2f EUR", preisProNacht);
  printf("\nNettoTotal: %.2f EUR", netTotal);
  printf("\nMwSt (7%): EUR %.2f", MwSt);
  printf("\n----------------------------------------------\n");
  printf("\ngrossTotal: %.2f EUR\n", grossTotal);

   return 0;
}
