#include <stdio.h>
#include <stdlib.h>

int main()

{
  system("clear");

// erstelle ein array das int werte aufnehmen kann.
// Zur runtime soll zunaechst die Groesse des arrays abgefragt werden
// Dann sollten die Werte von der Tastatur abgefragt werden
// dann das array anzeigen

  // Groesse des arrays abfragen
  int arrayLength;
  printf("\n Bitte Anzahl der Stellen fuer ein Array eingeben: ");
  scanf("%d", &arrayLength);

  // Werte in das array eingeben (mit einem loop)
  int zahlen1[arrayLength];
  for (int i=0; i<arrayLength; i++)
  {
    printf("\n Bitte Wert %d eingeben: ", i+1);
    scanf("%d", &zahlen1[i]);
  }

  // Ausgabe der Laenge und des arrays
  printf("\n Das Array hat %d Stellen und lautet: ", arrayLength);
  int j=0;
  while (j<arrayLength) // muss '<' sein '<=' funzt nicht
  {
    printf("%d | ", zahlen1[j]);
    j++;
  }


  printf("\n----------------------------------------------\n");


   return 0;
}
