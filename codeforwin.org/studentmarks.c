#include <stdio.h>
#include <stdlib.h>

int main()


// Write a C program to input marks (precentage of point) of five subjects Physics, Chemistry, Biology, Mathematics and Computer.
// Calculate percentage and grade according to given conditions:
// If percentage >= 90% : Grade A
// If percentage >= 80% : Grade B
// If percentage >= 70% : Grade C
// If percentage >= 60% : Grade D
// If percentage >= 40% : Grade E
// If percentage < 40% : Grade F


{
  system("clear");

  float physics, chemistry, biology, maths, cs;
  float average;

  printf("\n the marks for Physics (0-100): ");
  scanf("\n%f", &physics);
  printf("\n the marks for Chemistry (0-100): ");
  scanf("\n%f", &chemistry);
  printf("\n the marks for Bilogy (0-100): ");
  scanf("\n%f", &biology);
  printf("\n the marks for Mathematics (0-100): ");
  scanf("\n%f", &maths);
  printf("\n the marks for Computer Science (0-100): ");
  scanf("\n%f", &cs);

  if(physics>=90) {
    printf("\n Physics \tGrade: A");
  } else  if(physics>=80)
          {
            printf("\n Physics \tGrade: B");
          } else  if(physics>=70)
                  {
                    printf("\n Physics \tGrade: C");
                  } else  if(physics>=60)
                          {
                            printf("\n Physics \tGrade: D");
                          } else  if(physics>=400)
                                  {
                                    printf("\n Physics \tGrade: E");
                                  }
                                  else
                                  printf("\n Physics \tGrade: F");

  if(chemistry>=90) {
    printf("\n Chemistry \tGrade: A");
  } else  if(chemistry>=80)
          {
            printf("\n Chemistry \tGrade: B");
          } else  if(chemistry>=70)
                  {
                    printf("\n Chemistry \tGrade: C");
                  } else  if(chemistry>=60)
                          {
                            printf("\n Chemistry \tGrade: D");
                          } else  if(chemistry>=400)
                                  {
                                    printf("\n Chemistry \tGrade: E");
                                  }
                                  else
                                  printf("\n Chemistry \tGrade: F");

  if(biology>=90) {
    printf("\n Biology \tGrade: A");
  } else  if(biology>=80)
          {
            printf("\n Biology \tGrade: B");
          } else  if(biology>=70)
                  {
                    printf("\n Biology \tGrade: C");
                  } else  if(biology>=60)
                          {
                            printf("\n Biology \tGrade: D");
                          } else  if(biology>=400)
                                  {
                                    printf("\n Biology \tGrade: E");
                                  }
                                  else
                                  printf("\n Biology \tGrade: F");

  if(maths>=90) {
    printf("\n Mathematics \tGrade: A");
  } else  if(maths>=80)
          {
            printf("\n Mathematics \tGrade: B");
          } else  if(maths>=70)
                  {
                    printf("\n Mathematics \tGrade: C");
                  } else  if(maths>=60)
                          {
                            printf("\n Mathematics \tGrade: D");
                          } else  if(maths>=400)
                                  {
                                    printf("\n Mathematics \tGrade: E");
                                  }
                                  else
                                  printf("\n Mathematics \tGrade: F");

  if(cs>=90) {
    printf("\n Computer Science \tGrade: A");
  } else  if(cs>=80)
          {
            printf("\n Computer Science \tGrade: B");
          } else  if(cs>=70)
                  {
                    printf("\n Computer Science \tGrade: C");
                  } else  if(cs>=60)
                          {
                            printf("\n Computer Science \tGrade: D");
                          } else  if(cs>=400)
                                  {
                                    printf("\n Computer Science \tGrade: E");
                                  }
                                  else
                                  {
                                    printf("\n Computer Science \tGrade: F");
                                  }


  printf("\n----------------------------------------------\n");

  average = (physics+chemistry+biology+maths+cs)/5;

  if(average>=90) {
    printf("\n total \tGrade: A");
  } else  if(average>=80)
          {
            printf("\n total \tGrade: B");
          } else  if(average>=70)
                  {
                    printf("\n total \tGrade: C");
                  } else  if(average>=60)
                          {
                            printf("\n total \tGrade: D");
                          } else  if(average>=400)
                                  {
                                    printf("\n total \tGrade: E");
                                  }
                                  else
                                  printf("\n total \tGrade: F");

  printf("\n----------------------------------------------\n");

   return 0;
}
