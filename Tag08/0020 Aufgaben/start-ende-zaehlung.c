/*
	Programm, dass von einem Startwert bis zum Endwert aufwärts zählt und die Werte ausgibt
	Startwert & Endwert sollen abgefragt werden
	Es muss sichergestellt werden das Startwert kleiner als Endwert ist
*/

#include <stdio.h>
#include <stdlib.h>

int main()

{
  system("clear");

	int num1, num2, i, summe, tmp;

		printf("\nBitte num1 eingeben: ");
		scanf("%d", &num1);
		printf("\nBitte num2 eingeben: ");
		scanf("%d", &num2);


    if(num1==num2)
    {
      printf("\nbitte unterschiedliche zahlen eingeben");
    }

		if(num1>num2)
		{
			tmp=num1;
			num1=num2;
			num2=tmp;
		}

	i = num1;

	while(i <= num2 && num1 != num2)
	{
		printf("\n%d", i);
		i++;
    system("sleep 0.2s");
	}


  printf("\n----------------------------------------------\n");


   return 0;
}
