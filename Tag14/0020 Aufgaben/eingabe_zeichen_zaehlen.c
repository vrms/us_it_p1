#include <stdio.h>
#include <stdlib.h>

// Schreiben Sie ein Programm, das 20 Zeichen aus der Standardeingabe liest und nach Abschluss der Eingaben für jeden Buchstaben (Standardalphabet (a-z), Groß- und Kleinbuchstaben werden getrennt gezaehlt) ausgibt, wie oft er gelesen wurde.
//
// Entwickeln Sie einen Programmablaufplan oder ein Struktogramm oder ein Pseudocode und implementieren Sie es in C.

        //  |
        //  -->  innere Schleife vergleicht alle Stellen des Arrays mit dem index[x]
        //       zaehlt fuer jeden Gleichheitstreffer ('e' == 'e') den Zaehler hoch

int main()

{
	// Variable vom Typ int f. de Anzahl der Zeichen (=Groesse des arrays) deklarieren
	int array_groesse;

	// Anzahl der Zeichen vom user abfragen und in die Variable schreiben
	// printf("\n");
	scanf("%d", array_groesse);

	// array of char mit der Anzahl anlegen
	char zeichen[array_groesse];
	// die Zeichen vom user abfragen und in das array Schreiben
	// printf("\n");
	for(int i=0; i<array_groesse; i++)
	{
		scanf("%c", &zeichen[i]);
	}

	// 'aeussere' schleife
	// lies Zeichen in index[x] (z.B. 'e') und zaehle die indexstellen hoch
	char zeichen_zu_zaehlen;
	int zaehler;

	for(int i=0; i<array_groesse; i++)
	{
		// der zahler mus vor jeder innern schleife auf 0 gesetzt werden
		zaehler=0;
		zeichen_zu_zaehlen=zeichen[i];

		// innere Schleife --> das gesamte array wird udruchlaufen
		for(int j=0; j<array_groesse; j++)
		{
			// ueberall wo 'e'==array[i], zaehler hochsetzen
			if (zeichen_zu_zaehlen==zeichen[j])
			{
				zaehler++;
			}
		}
	printf("\n Anzahl dieses Zeichens: %d", zaehler);
	}
}
