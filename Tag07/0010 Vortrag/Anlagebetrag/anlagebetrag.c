#include <stdio.h>
#include <stdlib.h>

int main()

{
  system("clear");


  // defining some Variables

  float anlageBetrag;
  float zinsen;

  // printf("\n----------------------------------------------\n");

  printf("\nBitte Anlagebtrag eingeben: ");
  scanf("%f", &anlageBetrag );

  if(anlageBetrag > 50000)
  {
    zinsen = 2.0;
  }
    else if(anlageBetrag > 10000)
      {
        zinsen = 1.9;
      }
      else if(anlageBetrag > 5000)
        {
          zinsen = 1.7;
        }
  else
    zinsen = 1.5;

  printf("\n----------------------------------------------\n");
  printf("\n Anlagebetrag = EUR %.2f\n", anlageBetrag);
  printf("\n----------------------------------------------\n");
  printf("\n Ihr Zinssatz ist: %.2f%%", zinsen);
  printf("\n----------------------------------------------\n");

   return 0;
}
