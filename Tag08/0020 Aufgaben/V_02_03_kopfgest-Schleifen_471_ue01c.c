/*
	ue01:
	Erstellen Sie ein Programm, das von einem Startwert bis zu einem Endwert aufw�rts z�hlt und die Werte auf dem Bildschirm ausgibt.
	Startwert und Endwert sollen vom User abgefragt werden. Es muss sichergestellt werden, dass der Startwert kleiner als der Endwert ist.
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	// SetConsoleOutputCP(1252);
	// SetConsoleCP(1252);
	system("clear");

	int startwert, endwert;
	int schleifeAusfuehren=1;			// 0 bedeutet nicht ok (FALSE), 1 bedeutet ok (TRUE)

	printf("\nBitte Startwert eingeben: ");		// hier wird beliebiger Wert eingegeben
	scanf("%d", &startwert);

	// Vorschlag
	// endwert=startwert-1;		// mit dieser Vorbelegung von endwert k�men wir auf alle F�lle in die folgende Schleife




	/*
			Erlaeterung des folgenden loops:
			in den Loop wird eingetreten, wenn der Ausdruck der Bedingungen von while eine TRUE (1) enthaelt.
			Dies haben wir in l 17 so assigned, also gehen wir beim ersten Durchgang zwigned in den loop.
			in der if Sektion wird der Wert dann ggf. auf FALSE (0) geseszt --> in dem Fall treten wir aus dem loop aus.
	*/



	while(schleifeAusfuehren)
	{
		printf("\nBitte Endwert eingeben, der groesser ist als der Startwert: ");
		scanf("%d", &endwert);
		if(endwert>startwert)
		{
			schleifeAusfuehren=0;			// 0 bedeutet false --> Schleife wird nicht erneut aufgerufen
		}
	}

	while(startwert<=endwert)					// hier wird der Zahlenbereich ausgegeben
	{
		printf("\naktueller Wert ist : %d", startwert);
		startwert=startwert+1;
	}

	printf("\nEnde des Programms");

	// system("pause");
	printf("\n");
	return 0;
}
