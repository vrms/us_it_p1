#include <stdio.h>
#include <stdlib.h>

int main()
{
	system("chcp 1252");
	system("cls");
	
	int tagesZahl;
	
	printf("\nTageszahl eingeben: ");
	scanf("%d", &tagesZahl);
	
	switch(tagesZahl)
	{
		case 1:		printf("Montag");
					break;
		case 2:		printf("Dienstag");
					break;
		case 3:		printf("Mittwoch");
					break;
		case 4:		printf("Donnerstag");
					break;
		case 5:		printf("Freitag");
					break;
		case 6:		printf("Samstag");
					break;
		case 7:		printf("Sonntag");
					break;
		default:	printf("unbekannt");
					break;
	}
	return 0;
}
