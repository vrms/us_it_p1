#include <stdio.h>				
#include <windows.h>
#include <math.h>
#include <string.h>

int main()						
{
	SetConsoleOutputCP(1252);	
	SetConsoleCP(1252);			
	system("cls");				

	double x, y;
	double a=26.458;
	int b=87, z;
	
	x = sqrt(623);
	y = sqrt(17);

	printf("\nErgebnis x ist: %lf", x);
	printf("\nErgebnis y ist: %lf", y);
	
	x = sqrt(a);
	printf("\n\nErgebnis x ist: %lf", x);
	printf("\n-------------------------------------------------------------");
	
	x = sqrt(b);		// hier ist der �bergabe-Datentyp int --> funktioniert --> int ist als �bergabe-Datentyp kompatibel mit double
	printf("\n\nErgebnis x ist: %lf", x);
	printf("\n-------------------------------------------------------------");
	
	z = sqrt(b);		// hier ist der R�ckgabe-Datentyp int --> erzeugt keinen Compilier-Fehler --> aber vom Ergebnis werden die Nachkommastellen abgeschnittten
	printf("\n\nErgebnis z ist: %d", z);
	printf("\n-------------------------------------------------------------");
	
	x = pow(2,8);
	printf("\n\nErgebnis x ist: %lf", x);
	printf("\n-------------------------------------------------------------");
	
	a=5;
	y=21;
	x = pow(a,y);
	printf("\n\nErgebnis x ist: %lf", x);
	printf("\n-------------------------------------------------------------");
		
		
	printf("\n\nPI ist: %.21Lf", M_PI);		// M_PI ist eine Konstante, die in math.h definiert ist
	printf("\n-------------------------------------------------------------");
	
	
	// system("pause");
	printf("\n");
	return 0;
}