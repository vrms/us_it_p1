#include <stdio.h>
#include <stdlib.h>

int main()

{
  system("clear");

  printf("\n----------------------------------------------\n");

  int start, anzahl;

  printf("\n start und anzahl eingeben: ");
  scanf("\n %d", &start);
  scanf("\n %d", &anzahl);

  int current_value = start;
  int i = 0;

  while (i < anzahl)
  {
    printf("\n Wert: %d", current_value);
    i=i+1;
    current_value = current_value+1;
  }
  printf("\n----------------------------------------------\n");

   return 0;
}
