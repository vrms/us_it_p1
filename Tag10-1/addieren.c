#include <stdio.h>
#include <stdlib.h>

// program zur Summenberechnung
// Eingabe von 2 Zahlen
// Bildung der Summe aller Ganzzahlen zwischen der beiden Zahlen (inkl. der Grenzwerte)


int main()

{
  system("clear");


  int no1, no2;

  printf("\n Bitte geben Sie ein Zahl ein: ");
  scanf("%d", &no1);

  printf("\n Bitte geben Sie ein weitere Zahl ein: ");
  fflush(stdin);
  scanf("%d", &no2);

  int min, max;

  if (no1<no2) {
    min=no1;
    max=no2;
  } else {
    min=no2;
    max=no1;
  }

  int summe;

  printf("\n------- FOR loop ---------------------------------------\n");


  for (int i = min; i<=max; i++)
    if (i==min)
    {
      summe = i;
    }
    else
    {
      summe = summe + i;
    }

  printf("\n Summe = %d\n", summe);

  printf("\n------- WHILE loop -------------------------------------\n");

  int i = min;
  while (i <= max)
  {
    if(i==min)
    {
      summe = i;
      i++;
    }
    else
    {
      summe = summe +i;
      i++;
    }
  }

  printf("\n Summe = %d\n", summe);

  printf("\n------- DO/WHILE loop ----------------------------------\n");

  i = min;
  do
  {
    if(i==min)
    {
      summe = i;
      i++;=4050
    }
    else
    {
      summe = summe  +i;
      i++;
    }
  }
  while(i<=max);

  printf("\n Summe = %d\n", summe);

  printf("\n----------------------------------------------\n");

   return 0;
}
