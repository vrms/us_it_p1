/*
	ue03:
	Erstellen Sie ein Programm, das von einem Startwert bis zu einem Endwert r�ckw�rts z�hlt.
	Startwert und Endwert sollen vom User abgefragt werden. Es muss sichergestellt werden, dass der Startwert gr��er als der Endwert ist.
	Es sollen nur die Werte auf dem Bildschirm ausgegeben werden, die ohne Rest durch 3 teilbar sind.
*/

#include <stdio.h>				
#include <windows.h>

int main()						
{
	SetConsoleOutputCP(1252);	
	SetConsoleCP(1252);			
	system("cls");				

	int startwert, endwert;
	int schleifeAusfuehren=1;	
	
	printf("\nBitte Startwert eingeben: ");		
	scanf("%d", &startwert);	
	
	while(schleifeAusfuehren)
	{
		printf("\nBitte Endwert eingeben, der kleiner ist als der Startwert: ");
		scanf("%d", &endwert);
		if(endwert<startwert)
		{
			schleifeAusfuehren=0;			
		}
	}
	
	while(startwert>=endwert)					
	{
		// die folgende Ausgabe soll nur erfolgen, wenn der aktuelle startwert restlos durch 3 teilbar ist
		
		// if(startwert%3==0)		// f�r die Schleife entweder diesen ...
		if(!(startwert%3))			// oder diesen Schleifenkopf verwenden --> beide funktionieren
		{
			printf("\naktueller Wert ist : %d", startwert);
		}
		startwert=startwert-1;
	}
	
	/*
		Erl�uterungen zu Zeile 38 --> Schreibtischtest
		FALL 1: startwert ist 6 --> 6%3 ist 0, denn 6/3=2 Rest: 0 --> 0 entspricht FALSE --> Negierung mit ! --> Ausdruck ist TRUE --> Schleife wird ausgef�hrt
		FALL 2: startwert ist 7 --> 7%3 ist 1, denn 7/3=2 Rest: 1 --> 1 entspricht TRUE --> Negierung mit ! --> Ausdruck ist FALSE --> Schleife wird NICHT ausgef�hrt
		FALL 3: startwert ist 8 --> 8%3 ist 2, denn 8/3=2 Rest: 2 --> 2 entspricht TRUE --> Negierung mit ! --> Ausdruck ist FALSE --> Schleife wird NICHT ausgef�hrt
		FALL 4: startwert ist 9 --> 9%3 ist 0, denn 9/3=3 Rest: 0 --> 0 entspricht FALSE --> Negierung mit ! --> Ausdruck ist TRUE --> Schleife wird ausgef�hrt
	*/
	
	printf("\nEnde des Programms");
	
	// system("pause");
	printf("\n");
	return 0;
}