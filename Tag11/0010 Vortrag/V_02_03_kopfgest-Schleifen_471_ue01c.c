/*
	ue01:
	Erstellen Sie ein Programm, das von einem Startwert bis zu einem Endwert aufw�rts z�hlt und die Werte auf dem Bildschirm ausgibt.
	Startwert und Endwert sollen vom User abgefragt werden. Es muss sichergestellt werden, dass der Startwert kleiner als der Endwert ist.
*/

#include <stdio.h>				
#include <windows.h>

int main()						
{
	SetConsoleOutputCP(1252);	
	SetConsoleCP(1252);			
	system("cls");				

	int startwert, endwert;
	int schleifeAusfuehren=1;			// 0 bedeutet nicht ok (FALSE), 1 bedeutet ok (TRUE)
	
	printf("\nBitte Startwert eingeben: ");		// hier wird beliebiger Wert eingegeben
	scanf("%d", &startwert);
	
	// Vorschlag
	// endwert=startwert-1;		// mit dieser Vorbelegung von endwert k�men wir auf alle F�lle in die folgende Schleife
	
	/*
		Erl�uterung der unten folgenden Schleife:
		In die Schleife wird nur eingetreten, wenn der Ausdruck in der Klammer nach while ein logisches TRUE bzw. 1 enth�lt.
		Ist in der Klammer ein FALSE bzw. 0, wird die Schleife nicht ausgef�hrt, also �bersprungen.
		Im vorliegenden Bsp: die Variable schleifeAusfuehren ist mit 1 initialisiert, enth�lt also den logischen Wert TRUE -->
		beim ersten Mal kommen wir in jedem Fall in die Schleife rein. --> Endwert wird abgefragt --> Endwert wird mit
		if gepr�ft --> wenn der Endwert g�ltig ist, wird schleifeAusfuehren auf 0 gesetzt (FALSE) --> es wird in den Schleifenkopf
		zur�ckgesprungen und die Ausf�hrungsbedingung gepr�ft --> schleifeAusfuehren enth�lt FALSE --> Schleife wird nicht nochmals
		ausgef�hrt
		wenn der endwert noch nicht g�ltig ist, bleibt schleifeAusfuehren auf 1 (TRUE) und die Schleife wird erneut ausgef�hrt	
	*/
		
	while(schleifeAusfuehren)
	{
		printf("\nBitte Endwert eingeben, der gr��er ist als der Startwert: ");
		scanf("%d", &endwert);
		if(endwert>startwert)
		{
			schleifeAusfuehren=0;			// 0 bedeutet false --> Schleife wird nicht erneut aufgerufen
		}
	}
	
	while(startwert<=endwert)					// hier wird der Zahlenbereich ausgegeben
	{
		printf("\naktueller Wert ist : %d", startwert);
		startwert=startwert+1;
	}
	
	printf("\nEnde des Programms");
	
	// system("pause");
	printf("\n");
	return 0;
}