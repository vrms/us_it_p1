/*
	ue01:
	Erstellen Sie ein Programm, das von einem Startwert bis zu einem Endwert aufw�rts z�hlt und die Werte auf dem Bildschirm ausgibt.
	Startwert und Endwert sollen vom User abgefragt werden. Es muss sichergestellt werden, dass der Startwert kleiner als der Endwert ist.
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	// SetConsoleOutputCP(1252);
	// SetConsoleCP(1252);
	system("clear");

	int startwert, endwert, tmp;

	printf("\nBitte Startwert eingeben: ");		// hier wird beliebiger Wert eingegeben
	scanf("%d", &startwert);

	printf("\nBitte Endwert eingeben: ");		// hier wird beliebiger Wert eingegeben
	scanf("%d", &endwert);

	// ist der Endwert kleiner oder gleich Startwert --> wenn ja: endwert neu abfragen, bis der Endwert gr��er ist als der Startwert sonst: keine Aktion
	while(endwert<=startwert)
	{
		printf("\nBitte Endwert eingeben, der gr��er ist als der Startwert: ");
		scanf("%d", &endwert);
	}

	/*
	Dieser Tausch wird nicht mehr ben�tigt, da in der Schleife oben der Endwert so abgefragt wird, dass er gr��er als der Startwert ist.

	if(startwert>endwert)						// hier werden bei Bedarf die Werte getauscht
	{
		tmp=startwert;
		startwert=endwert;
		endwert=tmp;
	}
	*/

	while(startwert<=endwert)					// hier wird der Zahlenbereich ausgegeben
	{
		printf("\naktueller Wert ist : %d", startwert);
		startwert=startwert+1;
	}

	printf("\nEnde des Programms");

	// system("pause");
	printf("\n");
	return 0;
}
