#include <stdio.h>				
#include <windows.h>

int main()						
{
	SetConsoleOutputCP(1252);	
	SetConsoleCP(1252);			
	system("cls");

	float kosten[12][6] = {
							{45,56,78,124,234,78},
							{45.90,57.12,79.56,126.48,238.68,79.56},
							{46.82,58.26,81.15,129.01,243.45,81.15},
							{47.75,59.43,82.77,131.59,248.32,82.77},
							{48.71,60.62,84.43,134.22,253.29,84.43},
							{49.68,61.83,86.12,136.91,258.35,86.12},
							{50.68,63.07,87.84,139.64,263.52,87.84},
							{51.69,64.33,89.60,142.44,268.79,89.60},
							{52.72,65.61,91.39,145.29,274.17,91.39},
							{53.78,66.93,93.22,148.19,279.65,93.22},
							{54.85,68.26,95.08,151.16,285.24,95.08},
							{55.95,69.63,96.98,154.18,290.95,96.98}
						};

	printf("\n-------------------------------------------------------------");
	
	// erstmal array ordentlich = �bersichtlich ausgeben
	for(int zeilen = 0; zeilen < 12; zeilen++)
	{
		printf("\n");
		for(int spalten = 0; spalten < 6; spalten++)
		{
			printf("%10.2f ", kosten[zeilen][spalten]);
		}
	}
	
	// Summe aller Werte im array ermitteln
	printf("\n\nErmittlung der Summe aller Werte");
	float summeAllerWerte = 0;
	
	for(int zeilen = 0; zeilen < 12; zeilen++)
	{
		for(int spalten = 0; spalten < 6; spalten++)
		{
			summeAllerWerte = summeAllerWerte + kosten[zeilen][spalten];
		}
	}
	printf("\nSumme aller Werte: %f", summeAllerWerte);
	
	
	// Zusatzschritt: Ermitteln Sie die Summe der Kosten und den Mittelwert NUR f�r die Abteilung 1.
	
	
	// wir m�ssen f�r die Abt 1 alle Monate durchgehen und die Summe der Kosten errechnen
	// aus der Summe den Mittelwert errechnen

	printf("\n\nErmittlung der Summe f�r die Abteilung 1");
	float summeAbteilung = 0;
	for(int zeilen = 0; zeilen < 12; zeilen++)
	{
		summeAbteilung = summeAbteilung + kosten[zeilen][0];	
	}
	printf("\nSumme Abteilung 1: %f. Mittelwert: %f", summeAbteilung, summeAbteilung/12);

	// Ermittlung der Kosten und Mittelwerte f�r jede einzelne Abteilung mit Hilfe einer Schleife

	
	printf("\n\n-------------------------------------------------------------");
	for(int spalten = 0; spalten < 6; spalten++)	// Schleife f�r die Abteilungen
	{
		summeAbteilung=0;
		for(int zeilen = 0; zeilen < 12; zeilen++)					// Schleife f�r die 12 Monate
		{
			summeAbteilung = summeAbteilung + kosten[zeilen][spalten];	
		}
		printf("\nSumme Abteilung %d: %f. Mittelwert: %f", spalten+1, summeAbteilung, summeAbteilung/12);
	}

	// system("pause");
	printf("\n");
	return 0;
}