/*
	ue04:
	Erstellen Sie ein Programm, das von einem Startwert bis zu einem Endwert r�ckw�rts z�hlt.
	Startwert und Endwert sollen vom User abgefragt werden. Es muss sichergestellt werden, dass der Startwert gr��er als der Endwert ist.
	Es sollen nur die Werte auf dem Bildschirm ausgegeben werden, die ohne Rest durch 3 teilbar sind.
	Zus�tzlich soll am Ende des Programms ausgegeben werden, wieviel Werte auf dem Bildschirm ausgegeben wurden.
*/

#include <stdio.h>				
#include <windows.h>

int main()						
{
	SetConsoleOutputCP(1252);	
	SetConsoleCP(1252);			
	system("cls");				

	int startwert, endwert;
	int schleifeAusfuehren=1;	
	int zaehler=0;
	
	printf("\nBitte Startwert eingeben: ");		
	scanf("%d", &startwert);	
	
	while(schleifeAusfuehren)
	{
		printf("\nBitte Endwert eingeben, der kleiner ist als der Startwert: ");
		scanf("%d", &endwert);
		if(endwert<startwert)
		{
			schleifeAusfuehren=0;
		}
	}
	
	while(startwert>=endwert)					
	{
		// die folgende Ausgabe soll nur erfolgen, wenn der aktuelle startwert restlos durch 3 teilbar ist
		
		// if(startwert%3==0)		// f�r die Schleife entweder diesen ...
		if(!(startwert%3))			// oder diesen Schleifenkopf verwenden --> beide funktionieren
		{
			printf("\naktueller Wert ist : %d", startwert);
			zaehler=zaehler+1;		// zaehler++ --> diese Operation hei�t Inkrement
		}
		startwert=startwert-1;
	}
	
	printf("\nEs wurden %d Werte ausgegeben.", zaehler);
	printf("\nEnde des Programms");
	
	// system("pause");
	printf("\n");
	return 0;
}