#include <stdio.h>

int main()
{
	float summe_aller_stimmen;
	// die Summe ist zwar ein int-Wert, f�r die sp�tere Berechnung und 
	// Ausgabe muss er aber als float definiert werden
	float ergebnis1, ergebnis2, ergebnis3;
	
	summe_aller_stimmen=50+36+19;
	ergebnis1=50/summe_aller_stimmen*100;
	ergebnis2=36/summe_aller_stimmen*100;
	ergebnis3=19/summe_aller_stimmen*100; 
	
	// %d --> f�r Ganzzahl
	// %c --> f�r Zeichen
	// %f --> f�r Kommazahl
	
	printf("Summe: %4.0f ",summe_aller_stimmen);
	// %4.0f  --> %<gesamtzahl_der_stellen>.<anzahl_kommastellen>f
	
	printf("Summe: |%15.5f| ",summe_aller_stimmen);	

	
	printf("\nPerson 1 hat %.2f%% der Stimmen. ",ergebnis1);
	// .2 --> Begrenzung auf 2 Kommastellen
	// f�r die Darstellung des % --> %%
	printf("\nPerson 2 hat %.2f%% der Stimmen. ",ergebnis2);
	printf("\nPerson 3 hat %.2f%% der Stimmen. ",ergebnis3);
		
	return 0;
}