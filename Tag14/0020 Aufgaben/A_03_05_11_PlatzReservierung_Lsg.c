#include <stdio.h>
#include <stdlib.h>

int main()
{
    system("chcp 1252");
    system("cls");

    printf("\nTheater-Reservierung\n");

    char reservierung[6][15];
    int reihe=0, platz=0;
    char verlassen='n';

    // Initialisierung des Reservierungsarrays
    for(reihe=0;reihe<6;reihe++)
        for(platz=0;platz<15;platz++)
            reservierung[reihe][platz]='o';

    // diese while-Schleife wird abgearbeitet bis der Fall BREAK eintritt
    // Endlosschleife, da die 1 immer als TRUE ausgewertet wird
    while(1)		// 1 ist der Wahrheitswert TRUE --> dadurch haben wir eine Endlosschleife
    {
        printf("\naktueller Reservierungsstand\n\n");
        printf("           1  2  3  4  5  6  7  8  9  10 11 12 13 14 15");
        for(reihe=0;reihe<6;reihe++)
            {   printf("\nReihe: %d ",reihe+1);
                for(platz=0;platz<15;platz++)
                printf("%3c",reservierung[reihe][platz]);
            }
        // getchar();
        printf("\n\nWelchen Platz möchten Sie reservieren?");
        printf("\nReihe: ");
        fflush(stdin);
        scanf("%d",&reihe);
        printf("Platz: ");
        fflush(stdin);
        scanf("%d",&platz);
        if(reihe>6 || platz>15)
        {
            printf("\nfalsche Eingabe, bitte nochmal...");
            fflush(stdin);
            getchar();		// Programmablauf unterbrechen und auf Reaktion des Nutzers warten
            system("cls");
            continue; // die while-Schleife wird wieder von vorn abgearbeitet
        }
        if(reihe==0 || platz==0)
        {
            printf("\nProgramm verlassen j|n?");
            fflush(stdin);
            verlassen=getchar(); // getchar() liest genau ein Zeichen von der Tastatur
            if(verlassen='j')
            {
                system("cls");
                break;
            }
        }
        if(reservierung[reihe-1][platz-1]=='o')
            {
                reservierung[reihe-1][platz-1]='X';
                system("cls");
            }
        else
            {
                printf("leider nicht frei, bitte neue Eingabe ...");
                fflush(stdin);
                getchar();
                system("cls");
                continue;
            }
    }
    printf("\nEndgültiger Reservierungsstand\n\n");
    printf("           1  2  3  4  5  6  7  8  9  10 11 12 13 14 15");
    for(reihe=0;reihe<6;reihe++)
        {   printf("\nReihe: %d ",reihe+1);
            for(platz=0;platz<15;platz++)
            printf("%3c",reservierung[reihe][platz]);
        }

    printf("\n\n");
    return 0;

}
