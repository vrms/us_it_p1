#include <stdio.h>
#include <stdlib.h>

int main()
{
	system("chcp 1252");
	system("cls");

	int i, j, anzahl, laenge;
	char zeichen;

	printf("\nGeben Sie die Anzahl der einzugebenden Zeichen ein: ");
	scanf("%d", &laenge);

	char array1[laenge];


	printf("\n\nGeben Sie die Zeichen ein: \n");
	for(i=0;i<laenge;i++)
	{
		// fflush(stdin);
		printf("Zeichen %d: ",(i+1));
		scanf("%c",&array1[i]);
	}

	printf("\n");

	for(i=0;i<laenge;i++)
	{
		anzahl=0;
		zeichen=array1[i];

		for(j=0;j<laenge;j++)
			if(zeichen==array1[j]) anzahl++;

		printf("\nDas Zeichen %c kommt %dmal vor. ",array1[i],anzahl);
	}

	printf("\n\n");

	return 0;
}
