#include <stdio.h>
#include <stdlib.h>

  // Eine Wetterstation hat für die letzte Wochen folgende Temperaturen gemessen
    // Tag           1   2   3   4   5   6   7
    // Temperatur   25  22  18  12  15  19  23
  // Speichern sie die Temperaturen in einem sinnvollen Datentyp.
  // Welche Durchschnittstemperatur gab es in den 7 Tagen?
  // Welche war die höchste und welches die niedrigste Temperatur?
  // Welche beiden hintereinander folgende Tage haben die größte Temperaturschwankung? Geben sie die beiden Tage und die Temperaturschwankung aus.
  // Erstellen sie dazu einen PAP und ein Struktogramm.

  int main()

  {
    system("clear");

    float temperaturen[] = {25, 22, 18, 12, 15, 19, 23};
    float total = temperaturen[0];
    float mittelwert;
    float max=0;
    float min=100;

    for (int i=0; i<7; i++)
    {
      if (max<temperaturen[i]) {
        max=temperaturen[i];
      } else {
        max=max;
      }
      if (min>temperaturen[i]) {
       min=temperaturen[i];
      } else {
        min=min;
      }
      total= total + temperaturen[i+1];
    }

    mittelwert=total/7;


    float diff, diffMax;
    int from, to;

    // for (int i=0; i<7; i++)
    // {
    //   if (temperaturen[i]!=temperaturen[i++])
      // {
        // diffMax = abs(temperaturen[i] - temperaturen[i++]);
    //   }
    // }
    diffMax = abs(temperaturen[2] - temperaturen[3]); //this WORKS!!!

    printf("\n max = %.2f Grad\n", max);
    printf("\n min = %.2f Grad\n", min);
    // printf("\n Total = %.2f Grad\n", total);
    printf("\n Mittelwert = %.2f Grad\n", mittelwert);
    printf("\n maximale Different = %.2f Grad\n", diffMax);



   return 0;
}
