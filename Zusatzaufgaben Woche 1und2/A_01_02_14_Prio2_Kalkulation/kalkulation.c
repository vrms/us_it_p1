#include <stdio.h>
#include <stdlib.h>

int main()

{
  // system("clear");

  float angebot, qty, rabatt=0, skonto, zielEinkauf, barEinkauf, bezugsPreis, bezugsPreisTonne;
  float skontoProzent = 2, rabattProzent = 15;
  float bezugsKosten = 1450;

  printf("\n Bitte die Menge in Tonnen eingeben: ");
  scanf("%f", &qty);
  printf("\n Der Preis pro Tonne ist: ");
  scanf("%f", &angebot);


  // berechnungn Mengenrabatt
  if (qty>=5) {
    rabatt = angebot*qty * (rabattProzent/100);
  }


  zielEinkauf = angebot * qty - rabatt;           // zielEinkaufspreis berechnen
  skonto = zielEinkauf * skontoProzent/100;       // skonto berechnen
  barEinkauf = zielEinkauf - skonto;              // barEinkauf berechnen
  bezugsPreis = barEinkauf + bezugsKosten;        // bezugsPreis berechnen
  bezugsPreisTonne = bezugsPreis/qty;

  printf("\n----------------------------------------------\n");

  printf("\n der Mengenrabatt (%.2f%%) ist:\t$ %8.2f", rabattProzent/100, rabatt);
  printf("\n der zielEinkaufspreis ist:\t$ %8.2f", zielEinkauf);
  printf("\n der Bareinkaufspreis ist:\t$ %8.2f", barEinkauf);
  printf("\n Bezugskosten sind:\t\t$ %8.2f", bezugsKosten);
  printf("\n der Bezugspreis ist:\t\t$ %8.2f", bezugsPreis);
  printf("\n pro Tonne sind das:\t\t$ %8.2f", bezugsPreisTonne);

  printf("\n----------------------------------------------\n");

  return 0;
}
