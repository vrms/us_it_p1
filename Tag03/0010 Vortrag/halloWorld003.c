#include <stdio.h>				// hier werden sogenannte header-Dateien eingebunden
#include <windows.h>			// diese header-Dateien binden Funktionsbibliotheken ein, die im prgr genutzt werden k�nnen

int main()						// hier beginnt das eigentliche prgr. Der Bezeichner 'main' ist Pflicht
{
	SetConsoleOutputCP(1252);	// mit diesen Funktionen werden Zeichens�tze f�r Konsolenausgabe gesetzt
	SetConsoleCP(1252);			// ACHTUNG: Quelltext muss in ANSI kodiert sein
	
	system("cls");				// mit system() wird ein Terminalbefehl des OS aufgerufen
	// system("dir");
	
	printf("Hallo, world!\n");	// '\n' ist eine Steuerzeichen und erzeugt einen Zeilenumbruch
	printf("H�llo, w�rld! Ich wei�, da� �bermorgen Donnerstag ist.\n");
	
	// befehlirgendwas();		// unbekannter Befehl. erzeugt Fehlermeldng beim Kompilieren
	
	// ich bin ein einzeiliger Kommentar. Mit Strg +Q kann man in NP++ makrierte Zeieln auskommentieren
	
	/*
	Ich bin 
	ein mehrzeiliger
	Kommentarblock
	*/
	
	return 0;
}