#include <stdio.h>
#include <stdlib.h>

// Erstellen Sie ein Programm zur Berechnung der Quersummen von Zahlen.
//
// Das Programm soll die zu verarbeitende Zahl abfragen und anschließend die Quersumme berechnen und anzeigen. Anschließend soll das Programm fragen, ob weitere Zahlen verarbeitet werden sollen oder ob das Programm beendet werden soll.
//
// Erstellen Sie einen Programmablaufplan oder ein Struktogramm und ein C-Programm.

// Hinweis: Verwenden Sie für die Zerlegung der Zahl den Operator % und eine ganzzahlige Division durch 10.

int main()

{
  system("clear");

  int number, quersumme, cif1, cif2, cif3;

  // Eingabe 4 ziffrige Zahl

  printf("\n Bitte eine 3-stellige Numer eingeben: ");
  scanf("%d", &number);

  // ermitteln und Anzeige der Quersumme

  cif3 = number%10;
  cif2 = ((number-cif3)/10)%10;
  cif1 = ((number-cif3)/10)/10;

  quersumme = cif1 + cif2 + cif3;

  printf("\n %d | %d | %d", cif1, cif2, cif3);
  printf("\n Quersumme ist: %d", quersumme);
  // printf("\n Nummer ist: %d", number);

  // Abbruch vs neue Runde



  printf("\n----------------------------------------------\n");


   return 0;
}
