#include <stdio.h>
#include <stdlib.h>

int main()

{
  system("clear");

// erstelle ein array das float werte aufnehmen kann.
// Zur runtime soll zunaechst die Groesse des arrays abgefragt werden
// Dann sollten die Werte von der Tastatur abgefragt werden
// dann das array anzeigen

  // Groesse des arrays abfragen

  int arrayLength;
  float zahlen2[] = {5.67, 9.231, 11, 23.849, 2566.0};  // Deklarieren & Initialisieren des arrays
                                                        // inkl. Groessenfestlegung durch Angabe der Elemente
  for (int i=0; i<5; i++)
  {
    printf("%7.2f | ", i+1, zahlen2[i]);
  }

  printf("\n----------------------------------------------\n");

  float zahlen3[5] = {0};   // das array wird komplett mit 0 initialisiert. Die Groesse muss angegeben sein
  // float zahlen3[5] = {12};   // hier wird nur die erste Stelle (0) mit 12 initialisert. Der Rest mit 0.
  for (int i=0; i<5; i++)
  {
    printf("%7.2f | ", i+1, zahlen3[i]);
  }


  printf("\n----------------------------------------------\n");

  float zahlen4[3] = {5.67, 9.231, 11, 23.849, 2566.0};   //Liste ist groesser(5) als array (3)
  for (int i=0; i<3; i++)                                 //Compiler gibt eine Warnung, exe wird aber dennoch
  {                                                       //erzeugt (mit 3 Werten)
    printf("%7.2f | ", i+1, zahlen4[i]);
  }
  printf("\n----------------------------------------------\n");


   return 0;
}
