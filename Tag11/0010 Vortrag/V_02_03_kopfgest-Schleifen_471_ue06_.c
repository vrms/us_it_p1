#include <stdio.h>				
#include <windows.h>

int main()						
{
	SetConsoleOutputCP(1252);	
	SetConsoleCP(1252);			
	system("cls");

	int vr=1, mw=1;
	
	// Schleife f�r die Versuchsreihen von 1 bis 10 durchlaufen
	while(vr<=10)					// das ist die �u�ere Schleife
	{
		printf("\nVersuchsreihe %d \t", vr);
		
		// in jeder Schleife f�r eine Versuchsreihen jeweils die Me�werte von 1 bis 5 durchlaufen
		while(mw<=5)				// das ist die innere Schleife
		{
			printf(" mw: %d |", mw);
			mw=mw+1;
		}
		mw=1;
		vr=vr+1;
	}

	printf("\n-------------------------------------------------------------");

	// system("pause");
	printf("\n");
	return 0;
}