/* Aufgabenstellung: Sie sollen f�r eine Filiale den Jahresgesamtumsatz, den Monat mit dem geringsten Umsatz und den Monat mit dem h�chsten Umsatz ausgeben. Die jeweiligen Ums�tze pro Monat sollen eingegeben werden
Vorgehensweise
Welche Daten werden eingegeben, welche Daten ben�tige ich, um die Aufgabe zu l�sen?
Welche Schleife(n) bietet(n) sich an?
Einen Programmablaufplan, ein Struktogramm, Pseudocode  und den C-Code erstellen.
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	system("chcp 1252");
	system("cls");
	
	int monat_max=0, monat_max_nr=1, monat_min=0, monat_min_nr=1, jahresumsatz=0, umsatz=0;
	
	for(int monat=1;monat<=3;monat=monat+1)
	{
		printf("Umsatz Monat %2d eingeben: ",monat);
		fflush(stdin);
		scanf("%d", &umsatz);
		jahresumsatz=jahresumsatz+umsatz;
		if(monat==1)
		{
			monat_max=umsatz;
			monat_min=umsatz;
		}
		if(umsatz>monat_max) 
		{
			monat_max=umsatz;
			monat_max_nr=monat;
		}
		if(umsatz<monat_min) 
		{
			monat_min=umsatz;
			monat_min_nr=monat;
		}	
	}
	
	printf("\n\nDer Jahresgesamtumsatz ist:\t%6d",jahresumsatz);
	
	printf("\nDer kleinste Monatsumsatz ist:\t%6d im Monat %d",monat_min, monat_min_nr);
	
	switch (monat_min_nr)
	{
		case 1:	printf(" Januar");
				break;
		case 2:	printf(" Februar");
				break;
		case 3:	printf(" M�rz");
				break;
		case 4:	printf(" April");
				break;
		case 5:	printf(" Mai");
				break;
		case 6:	printf(" Juni");
				break;
		case 7:	printf(" Juli");
				break;
		case 8:	printf(" August");
				break;
		case 9:	printf(" September");
				break;
		case 10:	printf(" Oktober");
				break;
		case 11:	printf(" November");
				break;
		case 12:	printf(" Dezember");
				break;
		default: printf("\nunbekannter Monat");
				break;
	}
	
	printf("\nDer gr��te Monatsumsatz ist:\t%6d im Monat %d",monat_max, monat_max_nr);
	switch (monat_max_nr)
	{
		case 1:	printf(" Januar");
				break;
		case 2:	printf(" Februar");
				break;
		case 3:	printf(" M�rz");
				break;
		case 4:	printf(" April");
				break;
		case 5:	printf(" Mai");
				break;
		case 6:	printf(" Juni");
				break;
		case 7:	printf(" Juli");
				break;
		case 8:	printf(" August");
				break;
		case 9:	printf(" September");
				break;
		case 10:	printf(" Oktober");
				break;
		case 11:	printf(" November");
				break;
		case 12:	printf(" Dezember");
				break;
		default: printf("\nunbekannter Monat");
				break;
	}

	printf("\n\n");
//	system("pause");
	
	return 0;
}