4 Aufgaben

- Entwurf in PAP, Struktogramm, Pseudocode
- Entwicklung eines kleinen C-Programms auf der Basis eines vorliegenden Entwurfs.
  Der Entwurf kann sein PAP, Struktogramm oder Pseudocode.
- Benötigt wird der Umgang mit Variablen, Array, Verzweigungen und Schleifen.
