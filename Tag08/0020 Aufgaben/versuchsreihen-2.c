#include <stdio.h>
#include <stdlib.h>

int main()

{
  system("clear");

  int anzahlReihen;
  int anzahlWerte;

  printf("\n Bitte Anzahl Versuchreihen angeben: ");
  scanf("%d", &anzahlReihen);
  printf("\n Bitte Anzahl Messwerte angeben: ");
  scanf("%d", &anzahlWerte);

  int reihenNo=1, messwert=1;

  // loop for Versuchsreihen 1-10
  while (reihenNo<=anzahlReihen)
  {
    printf("\n  Versuchsreihe %2d\t---->\t|", reihenNo);

    // loop for Messwerte 1-5
    while (messwert<=anzahlWerte)
    {
      printf(" mw: %d | ", messwert);
      messwert++;  // benedet inneren horizontalen loop nach 5
    }
    messwert=1;   // laesst uns in l 18 wieder in den loop herein springen
    reihenNo++;
  }

  printf("\n\n");

   return 0;
}
