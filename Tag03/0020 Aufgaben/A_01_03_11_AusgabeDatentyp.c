/*

*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	system("chcp 1252");
	system("cls");

	int zahl=25;
	char buchstabe='B';			// einzelne Zeichen in Hochkommata 'x'
	float euroZuDollar=1.4588;
	float mwst1=19.0;
	float mwst2=7;
	
	printf("\nGanzzahl ist: %d",zahl);	// beachte Formatstring --> Ganzzahl --> d steht f�r decimal
	printf("\nGanzzahl ist: %i",zahl);	// gibt es einen Unterschied zwischen %d und %i ? --> nein --> Ganzzahl --> i steht f�r integer
	
	printf("\nbuchstabe ist: %c",buchstabe);

	printf("\neuroZuDollar ist: %f",euroZuDollar);

	printf("\nmwst1 ist: %f",mwst1);
	printf("\nmwst2 ist: %f",mwst2);

	// optional:
	printf("\n\nbuchstabe ist: %c. Der entsprechende ASCII-Code ist: %d",buchstabe, buchstabe);
	printf("\nzahl ist dezimal: %d | hexadezimal: %x | oktal: %o", zahl, zahl, zahl);
	printf("\nmwst1 ist: %7.2f",mwst1);
	printf("\nmwst2 ist: %7.2f",mwst2);

	
	printf("\n\n");
	// return 0;
	return EXIT_SUCCESS;	// Konstante kommt aus der stdlib.h
}