#include <stdio.h>				
#include <windows.h>

int main()						
{
	SetConsoleOutputCP(1252);	
	SetConsoleCP(1252);			
	system("cls");

	printf("\n-------------------------------------------------------------");
	printf("\narrays");
	printf("\n-------------------------------------------------------------");
	
	// array of char
	char name[5] = {'W', 'e', 'b', 'e', 'r'};	// Deklaration und Initialisierung des arrays
	
	// VARIANTE 1
	printf("\nDer Name ist: %c%c%c%c%c", name[0], name[1], name[2], name[3], name[4]);
	
	
	// VARIANTE 2
	printf("\n\nDer Name ist: ");
	
	for(int i=0; i<5; i++)		// im Schleifenkopf wird der Index von 0 bis 4 durchlaufen
	{
		printf("%c", name[i]);	// 	hier wird mit jedem Schleifendurchlauf genau ein Zeichen ausgegeben
	}
	
	// geben Sie rewe aus --> HINWEIS: hier funktioniert eine Schleife sehr schlecht
	printf("\nDer Name ist: %c%c%c%c", name[4], name[1], name[0], name[1]);
	
	// korrigieren Sie das gro�e w in ein kleiner w und geben Sie rewe erneut aus --> �ndern Sie das Array !!!
	name[0] = 'w';
	printf("\nDer Name ist: %c%c%c%c", name[4], name[1], name[0], name[1]);
	
	printf("\n-------------------------------------------------------------");
	
	// array of int
	
	// Erstellen Sie ein Array, das int-Werte aufnehmen kann. Zur Laufzeit des Programms soll zun�chst die Gr��e
	// des Arrays abgefragt werden. Dann sollen die Werte von der Tastatur abgefragt und in das Array eingetragen
	// werden. Zur Kontrolle soll das array ausgegeben werden.
	
	// Gr��e des Arrays abfragen und in einer Variablen groesse speichern
	int groesse;
	printf("\nBitte Gr��e eingeben: ");
	scanf("%d", &groesse);
	
	// array unter Beachtung der Variablen groesse deklarieren
	int zahlen1[groesse];
	
	// wir brauchen eine Schleife f�r die Eingabe --> Anzahl der Schleifendurchl�ufe = groesse
	// Werte in das Array eingeben BEACHTE: Zugriff auf Array-Stelle �ber INDEX
	for(int i=0; i < groesse; i++)
	{
		printf("\nBitte Wert an der Stelle mit dem Index %d eingeben: ", i);
		scanf("%d", &zahlen1[i]);		// & wird DOCH ben�tigt	
		printf(" (Das ist der %d. Wert.)", i+1);
	}
	
	// Ausgabe des arrays �ber eine Schleife
	printf("\n\n");
	for(int i=0; i < groesse; i++)
	{
		printf("\nDas ist der %d. Wert: %d", i+1, zahlen1[i]);
	}

	// system("pause");
	printf("\n");
	return 0;
}