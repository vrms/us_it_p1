/* Aufgabenstellung: Sie sollen f�r eine Filiale den Jahresgesamtumsatz, den Monat mit dem geringsten Umsatz und den Monat mit dem h�chsten Umsatz ausgeben. Die jeweiligen Ums�tze pro Monat sollen eingegeben werden
Vorgehensweise
Welche Daten werden eingegeben, welche Daten ben�tige ich, um die Aufgabe zu l�sen?
Welche Schleife(n) bietet(n) sich an?
Einen Programmablaufplan, ein Struktogramm, Pseudocode  und den C-Code erstellen.
*/

start_program jahresumsatz

	monat_max=0
	monat_min=0
	jahresumsatz=0

	for(monat=1;monat<=12;monat=monat+1)
		umsatz=eingabe()
		jahresumsatz=jahresumsatz+umsatz
		wenn(monat==1)
			dann
				monat_max=umsatz
				monat_min=umsatz
				monat_max_nr=monat
				monat_min_nr=monat
		end_wenn
		wenn(umsatz>monat_max)
			dann monat_max=umsatz
		end_wenn
		wenn(umsatz<monat_min)
			dann monat_min=umsatz
		end_wenn
	end_for

	ausgabe(jahresumsatz)

	ausgabe(Monat mit dem h�chsten Umsatz ist: )
	switch monat_max
		1:	Ausgabe(Januar)
		2:	Ausgabe(Februar)
		3:	Ausgabe(M�rz)
		12: Ausgabe(Dezember)
		default: Ausgabe(Monat unbekannt)
	end_switch
	// ausgabe(monat_max)

	ausgabe(Monat mit dem geringsten Umsatz ist: )
	switch monat_min
		1:	Ausgabe(Januar)
		2:	Ausgabe(Februar)
		3:	Ausgabe(M�rz)
	end_switch
	// ausgabe(monat_min)

end_program
