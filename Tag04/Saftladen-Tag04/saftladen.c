# include <stdio.h>
# include <stdlib.h>

int main()
{

  system("clear");

  // defining some variables
  float aepfelProLiter = 1.5;
  float birnenProLiter = 1.2;
  float aepfelPreisKg = 0.85;
  float birnenPreisKg = 0.95;

  // Calculation
  float aepfelPreisLiter = aepfelProLiter * aepfelPreisKg;
  float birnenPreisLiter = birnenProLiter * birnenPreisKg;

  // if/esle sequence
  if (aepfelPreisLiter == birnenPreisLiter)  //check equality
  {
    printf("\nApfelsaft und Birnensaft ist in der Herstellung gleich teuer (%.2f/Liter).", aepfelPreisLiter);
  }

  else
    if(aepfelPreisLiter > birnenPreisLiter)  //Apfelsaft teurer als Birnensaft
    {
      printf("\nDer Herstellungspreis pro Liter Birnensaft (EUR %.2f) ist niedriger als der fuer Apfelsaft (EUR %.2f/Liter).", birnenPreisLiter, aepfelPreisLiter);
    }
    else
    {
      printf("\nDer Herstellungspreis pro Liter Apfelsaft (EUR %.2f) ist niedriger als der fuer Birnensaft (EUR %.2f/Liter).", aepfelPreisLiter, birnenPreisLiter);
    }

  printf("\n\n");

  return 0;
}
