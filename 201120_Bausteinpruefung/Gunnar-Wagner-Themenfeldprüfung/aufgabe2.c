// Erstellen Sie zu folgendem Sachverhalt den Pseudocode:
//
//     • Das Programm fragt vom User ab, wie viele ganze Zahlen er im Folgenden eingeben möchte.
//     • Anschließend wird der User dann entsprechend genauso viele Eingaben vornehmen können.
//     • Nach dieser Eingabe-Schleife soll das Maximum aller vom User eingegebenen Werte auf der Konsole ausgegeben werden und das Programm enden.
//
// HINWEIS: Geben Sie im Pseudocode die verwendeten Variablen mit Datentyp an.
//
//
// =================================================================================

#include <stdio.h>
#include <stdlib.h>

int main()

{
  system("clear");

  int anzahl, nummer;
  int ergebnis=0;

  printf("\n Bitte Anzahl der zu addierenden Ganzzahlen eingeben: ");
  scanf("\n %d", &anzahl );

  int i = 1;

  for(i<anzahl; i=anzahl; i++)
  {
    printf("\n Bitte eine ganzzahl eingeben: ");
    scanf("\n %d", &nummer);
    ergebnis = ergebnis + nummer;
  }

  printf("\n Ergebnis: ", ergebnis);


  printf("\n----------------------------------------------\n");


   return 0;
}
