/*Sie sollen ein Programm entwickeln, das drei Ganzzahlen als Eingabe erh�lt und pr�ft, ob alle drei Zahlen unterschiedliche letzte Ziffern besitzen. Geben Sie das  Ergebnis auf der Konsole aus.*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	int zahl1, zahl2, zahl3, lz1, lz2,lz3;
	
	system("chcp 1252");
	// ACHTUNG: Den Quelltext mit ANSI-Kodierung abspeichern lassen, damit die Codepage passt.
	system("cls");
	
	printf("\nZahl 1 eingeben: ");
	scanf("%d", &zahl1);

	printf("\nZahl 2 eingeben: ");
	fflush(stdin);		// Tastaturpuffer vor der neuen Eingabe l�schen lassen
	scanf("%d", &zahl2);
	
	printf("\nZahl 3 eingeben: ");
	fflush(stdin);		// Tastaturpuffer vor der neuen Eingabe l�schen lassen
	scanf("%d", &zahl3);

	lz1=zahl1%10;
	lz2=zahl2%10;
	lz3=zahl3%10;
	
	// if((lz1!=lz2)&&(lz1!=lz3)&&(lz2!=lz3))
	if((lz1!=lz2)&&(lz1!=lz3))			
	{
		printf("\nalle l�tzten Z�ffern sind unterschiedlich: %d, %d, %d", lz1, lz2, lz3);
	}
	else
	{
		printf("\nes gibt gleiche letzte Ziffern: : %d, %d, %d", lz1, lz2, lz3);
	}
	
	printf("\n\n");
	system("pause");
	
	return 0;
}

