/*
Sie sollen ein Programm entwickeln, mit dem ermittelt wird, mit welchen Aufz�gen eine Last transportiert werden kann.
1.	In den ersten Aufzug passen nur Lasten, die nicht schwerer als 50 Kg sind und nicht h�her als 1,5 Meter sind.
2.	Im zweiten Aufzug k�nnen Lasten bis 125 Kg bef�rdert werden, die nicht h�her als 2,5 Meter sind
3.	Im dritte Aufzug k�nnen Lasten bis 500Kg bef�rdert werden, aber nur, wenn sie nicht h�her als 2 Meter sind

Als Eingabe bekommen Sie das Gewicht und die H�he der zu transportierenden Last.
Erstellen Sie zuerst den Pseudocode und entwickeln Sie anschlie�end das C Programm

BEACHTE: Welcher Aufzug soll gew�hlt werden: Alle m�glichen oder nur der erste, der passt?

*/

#include <stdio.h>				
#include <windows.h>

int main()						
{
	SetConsoleOutputCP(1252);	
	SetConsoleCP(1252);			
	system("cls");				

	float masse, hoehe;
	
	printf("\nMasse eingeben: ");
	scanf("%f", &masse);
	printf("H�he eingeben: ");
	scanf("%f", &hoehe);
	
	printf("\n-------------------------------------------------------------");
	if(masse<=50 && hoehe<=1.5)
		printf("\nAufzug 1 geeignet");
	else
		printf("\nAufzug 1 NICHT geeignet");

	if(masse<=125 && hoehe<=2.5)
		printf("\nAufzug 2 geeignet");
	else
		printf("\nAufzug 2 NICHT geeignet");
	
	if(masse<=500 && hoehe<=2)
		printf("\nAufzug 3 geeignet");
	else
		printf("\nAufzug 3 NICHT geeignet");
		
	printf("\n-------------------------------------------------------------");

	// system("pause");
	printf("\n");
	return 0;
}