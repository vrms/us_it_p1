#include <stdio.h>				
#include <windows.h>

int main()						
{
	SetConsoleOutputCP(1252);	
	SetConsoleCP(1252);			
	system("cls");

	printf("\n-------------------------------------------------------------");
	printf("\nz�hlergesteuerte Schleifen");
	printf("\n-------------------------------------------------------------");

	// z�hlen Sie bis 12 mit Bildschirmausgabe
	
	int i;			// i steht f�r Iteration ODER k�nnte f�r Inkrement stehen (kann aber einen beliebigen Namen haben)
	
	
	// for ( i=1 ; 							i<=12 ; 							i=i+1 )
	//		Setzen und initialisieren 		Ausf�hrungsbedingung				Anweisung f�r die Schleifenvariablen
	//		der Schleifenvariablen			solange Ausdruck wahr ist			NACH jedem Schleifendurchlauf
	//		nur beim 1. Eintritt			VOR jedem Durchlauf
	//										das ist ein logischer Ausdruck
	//										opt. mit Klammern, Vergleich und 
	//										logischen Operatoren
	
	for( i=1 ; i<=12 ; i=i+1 )		// Schleifenkopf
	{
		// Schleifenk�rper
		printf("\ni = %d", i);
	}
	
	printf("\n");
	
	for( i=0 ; i>=-20 ; i=i-1 )		// Schleifenkopf
	{
		// Schleifenk�rper
		printf("\ni = %d", i);
	}
	printf("\n-------------------------------------------------------------");
	
	for(int j = 45; j<= 55; j=j+2)	// j++            j=j++  ???
	{
		printf("\nj = %d", j);
	}
	//HINWEIS: j ist ein lokale Variable NUR f�r diese Schleife
	
	// printf("\nj = %d", j);
	// diese Anweisung funktioniert nicht, da j hier nicht bekannt ist (sondern nur in der Schleife oben)

	printf("\n-------------------------------------------------------------");
	
	for(float m=4.239; m<30; m=m+3.086)
	{
		printf("\nm = %8.3f", m);
	}

	printf("\n-------------------------------------------------------------");

	for(float m=4.239; m<30; m=m*1.65)
	{
		printf("\nm = %8.3f", m);
	}
	// Die Schleifenvariable kann einen beliebigen Basis-Datentyp (int, float, double) haben. Die Berechnung
	// des Schleifendurchlaufs mit der Variablen kann mit beliebigen (nat�rlich sinnvollen) Operationen erfolgen (+ - * /)
	
	printf("\n\n-------------------------------------------------------------");
	printf("\nSprunganweisungen in Schleifen");
	printf("\n-------------------------------------------------------------");	

	// in der folgende Schleife soll der Bereich von 15 bis 25 �bersprungen werden
	for(float m=4; m<40; m=m+3)
	{
		if(m>=15 && m<=25)
		{
			continue;
		}
		printf("\nm = %8.3f", m);
	}
	// continue bedeutet, dass der aktuelle Schleifendurchlauf abgebrochen und mit dem n�chsten Schleifendurchlauf fortgesetzt wird --> bei continue wird der Rest des Schleifenk�rpers �bersprungen und der n�chste Durchlauf gestartet
	// mit continue wird die UNMITTELBAR �BERGEORDNETE Schleife abgebrochen und mit dem n�chsten Schritt fortgesetzt
	// zu beachten bei geschachtelten Schleifen

	printf("\n-------------------------------------------------------------");

	// in der folgende Schleife soll bei Werten �ber 15 die Schleife beendet werden
	for(float m=4; m<40; m=m+3)
	{
		if(m>=15)
		{
			break;
		}
		printf("\nm = %8.3f", m);
	}
	printf("\nDie Schleife wurde mit break verlassen.");
	// break bedeutet, dass der aktuelle Schleifendurchlauf beendet und der Rest des Schleifenk�rpers �bersprungen wird. Das Programm wird  NACH der Schleife fortgesetzt.
	// BREAK wirkt nur auf die UNMITTELBAR �BERGEORDNETE Schleife
	
	// CONTINUE und BREAK k�nnen in ALLEN Schleifen verwendet werden
	
	printf("\n-------------------------------------------------------------");
	
	for(int m=1, n=5 ; m<20 && n<30 ; m++, n=n+2)
	{
		printf("\nm ist %d und n ist %d ", m, n);
	}
	// BESONDERHEIT: in dieser Variante werden im Schleifenkopf mehrere Variablen verwendet
	
	printf("\n-------------------------------------------------------------");
	
	i=10;							// das ist technisch m�glich, aber sehr, sehr un�bersichtlich
	for( ; i<20 ; i=i+2)
	{
		printf("\ni ist %d ", i);
		i=i-1;						// das ist technisch m�glich, aber sehr, sehr un�bersichtlich
	}								// nicht alles was geht ist auch gut!!!
		

	// system("pause");
	printf("\n");
	return 0;
}