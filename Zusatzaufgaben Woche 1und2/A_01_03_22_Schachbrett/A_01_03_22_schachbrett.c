#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main()
{
	system("chcp 1252");
	system("cls");
	float flaeche, seitenlaenge;
	
	printf("\nBitte Fl�che eingeben: \t");
	fflush(stdin);
	scanf("%f",&flaeche);

	seitenlaenge=sqrt(flaeche);
	// sqrt  --> square root
	
	printf("\nFl�che:\t\t%10.4f",flaeche);
	printf("\nSeitenl�nge:\t%10.4f",seitenlaenge);
	printf("\nKontrolle:\t%10.4f",seitenlaenge*seitenlaenge);
	
	return 0;
}