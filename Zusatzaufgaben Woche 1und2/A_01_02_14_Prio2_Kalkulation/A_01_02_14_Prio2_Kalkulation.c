/*

*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main()
{
	system("chcp 1252");
	system("cls");
	
	float ek_preis, ek_preis_gesamt, menge, mengen_rabatt_proz=15., mengen_rabatt_proz_neu, zek_preis, skonto_proz=2., skonto_absolut, mengen_rabatt_absolut;
	float barEinkauf, bezugsKosten=1450., bezugsKostenNeu=0., bezugsPreisGesamt, bezugsPreisTonne;
	
	printf("\n--------------------------------------");
	// ek_preis
	printf("\nEingabe Einkaufspreis je t: ");
	scanf("%f",&ek_preis);
	// printf("\t\tEingabe war: %f", ek_preis);
	
	printf("\n--------------------------------------");
	// menge
	printf("\nEingabe Menge in t: ");
	scanf("%f",&menge);
	// printf("\t\tEingabe war: %f", menge);
	
	printf("\n--------------------------------------");
	// mengen_rabatt
	printf("\nDer pauschale Mengenrabatt in %% ist: %f", mengen_rabatt_proz);	
	printf("\nGeben Sie bei Bedarf einen neuen Wert ein (sonst -1): ");
	scanf("%f",&mengen_rabatt_proz_neu);
	//printf("\t\tEingabe war: %f", mengen_rabatt_proz_neu);	
	if(mengen_rabatt_proz!=mengen_rabatt_proz_neu && mengen_rabatt_proz_neu!=-1)
	{
		mengen_rabatt_proz=mengen_rabatt_proz_neu;
	}
	printf("\nMengenrabatt ist: %f", mengen_rabatt_proz);
	
	printf("\n--------------------------------------");
	// bezugsKosten
	printf("\nDie pauschalen Bezugskosten sind: %f", bezugsKosten);	
	printf("\nGeben Sie bei Bedarf einen neuen Wert ein (sonst -1): ");
	scanf("%f",&bezugsKostenNeu);
	//printf("\t\tEingabe war: %f", bezugsKostenNeu);	
	if(bezugsKosten!=bezugsKostenNeu && bezugsKostenNeu!=-1)
	{
		bezugsKosten=bezugsKostenNeu;
	}
	printf("\nBezugskosten sind: %f", bezugsKosten);

	printf("\n\n-------------------------------------------");
	printf("\n----------  Kalkulation  ------------------");
	ek_preis_gesamt=ek_preis*menge;
	printf("\n\tEinkaufspreis:\t\t%8.2f", ek_preis_gesamt);
	mengen_rabatt_absolut=ek_preis_gesamt*mengen_rabatt_proz/100;
	printf("\n-\tMengenrabatt:\t\t%8.2f", mengen_rabatt_absolut);
	zek_preis=ek_preis_gesamt-mengen_rabatt_absolut;
	printf("\n=\tZielkaufspreis:\t\t%8.2f", zek_preis);
	skonto_absolut=zek_preis*skonto_proz/100;
	printf("\n-\tSkonto:\t\t\t%8.2f", skonto_absolut);
	barEinkauf=zek_preis-skonto_absolut;
	printf("\n=\tBareinkaufspreis:\t%8.2f", barEinkauf);
	printf("\n+\tBezugskosten:\t\t%8.2f", bezugsKosten);
	bezugsPreisGesamt=barEinkauf+bezugsKosten;
	printf("\n=\tBezugspreis Gesamt:\t%8.2f", bezugsPreisGesamt);
	bezugsPreisTonne=bezugsPreisGesamt/menge;
	printf("\n=\tBezugspreis je Tonne:\t%8.2f", bezugsPreisTonne);
	printf("\n-------------------------------------------");
	printf("\n\n");
	return 0;
}


