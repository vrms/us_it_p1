﻿#include <stdio.h>

int main()
{
	float zahl1, zahl2, ergebnis;
	char operator;
	int operatorUnbekannt;
	printf("Zahl1 eingeben:");
	scanf("%f", &zahl1);
	fflush(stdin);
	printf("Zahl2 eingeben:");
	scanf("%f", &zahl2);
	fflush(stdin);
	do
	{
		printf("Operator eingeben:");
		scanf("%c", &operator);
		fflush(stdin);
		operatorUnbekannt = 0;
		switch(operator)
		{
			case '+': ergebnis = zahl1 + zahl2;
			          printf("Ergebnis:%.2f", ergebnis);
					  break;
			case '-':ergebnis = zahl1 - zahl2;
			         printf("Ergebnis:%.2f", ergebnis);
					 break;
			case '*':ergebnis = zahl1 * zahl2;
			         printf("Ergebnis:%.2f", ergebnis);
					 break;
			case '/':if(zahl2 != 0)
			         {
						 ergebnis = zahl1 / zahl2;
						 printf("Ergebnis:%.2f", ergebnis);
			         }
					 else
					 {
						 operatorUnbekannt = 1;
						 printf("Nenner darf nicht 0 sein\n");
					 }
					 break;
			case '%':if(zahl2 > 0)
			         {
						 ergebnis = (int)zahl1 % (int)zahl2;
						 printf("Ergebnis:%.2f", ergebnis);
			         }
					 else
					 {
						 operatorUnbekannt = 1;
						 printf("Nur Zahlen groesser 0 sind bei Modulo erlaubt\n");
					 }
					 break;
			default: operatorUnbekannt = 1;
					 printf("unbekannter Operator\n");
		}
	} while (operatorUnbekannt);
	return 0;
}
