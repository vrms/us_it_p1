#include <stdio.h>
#include <stdlib.h>

int main()

{
  system("clear");

  float zinssatz, summeZinsen, jahresZinsen, ausZahlung;
  int einlage, laufzeit, gelaufeneZeit=1;

  printf("\nBitte geben Sie das EinlageKapital ein: ");
  scanf("%d", &einlage);
  printf("\nBitte geben Sie den Zinssatz ein: ");
  scanf("%f", &zinssatz);
  printf("\nWelche Laufzeit soll die Anlage haben?: ");
  scanf("%d", &laufzeit);

  while (gelaufeneZeit<=laufzeit)
  {
    printf("\n  Jahr %2d\t---->\t|", gelaufeneZeit);
    jahresZinsen = (einlage * zinssatz/100);
//    printf(" einlage %.2f | Jahreszinsen: %.2f", einlage, jahresZinsen);
    printf(" Jahreszinsen: %.2f", jahresZinsen);
    summeZinsen = jahresZinsen;
    gelaufeneZeit++;
    einlage = einlage + jahresZinsen;
  }

  ausZahlung = einlage + summeZinsen;
  printf("\n Auszahlungsbetrag: %.2f", ausZahlung);


  // do {
  //   jahresZinsen = (einlage * (1+zinssatz/100));
  //   einlage = ausZahlung;
  //   gelaufeneZeit++;
  // } while(gelaufeneZeit<laufzeit);

  // printf("%d / %.2f%% / %d / Auszahlung: %.2f", einlage, zinssatz, laufzeit, ausZahlung);

  printf("\n----------------------------------------------\n");

  return 0;
}
