/*
	ue02:
	Erstellen Sie ein Programm, das von einem Startwert bis zu einem Endwert rueckwaerts zaehlt und die Werte auf dem Bildschirm ausgibt.
	Startwert und Endwert sollen vom User abgefragt werden. Es muss sichergestellt werden, dass der Startwert groesser als der Endwert ist.
	Nur Ergebnisse ausgeben, die ohne Rest durch 3 teilbar ist!
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	// SetConsoleOutputCP(1252);
	// SetConsoleCP(1252);
	system("clear");

	int startwert, endwert, rest;
	int schleifeAusfuehren=1;			// 0 bedeutet nicht ok (FALSE), 1 bedeutet ok (TRUE)

	printf("\nBitte Startwert eingeben: ");		// hier wird beliebiger Wert eingegeben
	scanf("%d", &startwert);

	while(schleifeAusfuehren)
	{
		printf("\nBitte Endwert eingeben, der kleiner ist als der Startwert: ");
		scanf("%d", &endwert);
		if(endwert<startwert)
		{
			schleifeAusfuehren=0;			// 0 bedeutet false --> Schleife wird nicht erneut aufgerufen
		}
	}

	while(startwert>=endwert)					// hier wird der Zahlenbereich ausgegeben
	{
			// Ausgabe soll nur erfolgen wenn Wert durch 3 teilbar ist
		if(startwert%3==0)
		// if(!(startwert%3))		//funktioniert auch, ist aber schwieriger zu erfassen
		{
			printf("\naktueller Wert ist: %d", startwert);
			system("sleep 0.1s");
		}
		startwert=startwert-1;
	}

	printf("\n\nEnde des Programms\n");

	// system("pause");
	printf("\n");
	return 0;
}
