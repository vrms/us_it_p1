# include <stdio.h>
# include <stdlib.h>

int main()
{

  system("clear");

  // defining some variables
  float no1, no2;
  float sumUser, multiUser, divisionUser;
  float sum, multi, division;
  float correct, correctPercent;

  printf("\n-----------------------------------------------------------------------------");

  // getting user input
  printf("\nGeben Sie bitte eine Zahl ein: ");
  scanf("%f", &no1);
  printf("\nGeben Sie bitte eine weitere Zahl ein: ");
  scanf("%f", &no2);

  printf("\nGeben Sie bitte die Summe beider Zahlen ein: ");
  scanf("%f", &sumUser);
  printf("\nGeben Sie bitte das Ergebnis einer Multiplikation beider Zahlen ein: ");
  scanf("%f", &multiUser);
  printf("\nGeben Sie bitte das Ergebnis der Division (Zahl 1 / Zahl2) beider Zahlen ein: ");
  scanf("%f", &divisionUser);

  // the basic caculations
  sum = no1 + no2;
  multi = no1 * no2;
  division = no1 / no2;

  // 3 if/esle sequences
  printf("\n-----------------------------------------------------------------------------");

  if(sumUser==sum)  //checking the addition
  {
    printf("\nDas Ergebnis Ihrer Addition (%.2f) ist korrekt!", sum);
    correct = +1;   //adds 1 to the value for 'correct'
  }

  else
  {
    printf("\nDas Ergebnis Ihrer Addition (%.2f) leider falsch! Das richtige Ergebnis lautet: %.2f", sumUser, sum);
  }

  printf("\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");

  if(multiUser==multi)  //checking the multiplication
  {
    printf("\nDas Ergebnis Ihrer Multiplikation (%.2f) ist korrekt!", multi);
    correct = correct + 1;
  }

  else
  {
    printf("\nDas Ergebnis Ihrer Multiplikation (%.2f) leider falsch! Das richtige Ergebnis lautet: %.2f", multiUser, multi);
  }

  printf("\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");


  if(divisionUser==division)  //checking the division
  {
    printf("\nDas Ergebnis Ihrer Division (%.2f) ist korrekt!", division);
    correct = correct + 1;
  }

  else
  {
    printf("\nDas Ergebnis Ihrer Division (%.2f) leider falsch! Das richtige Ergebnis lautet: %.2f", divisionUser, division);
  }

  correctPercent = correct / 3 * 100;

  printf("\n===========================================================================");
  printf("\nSie haben %.0f von 3 Ergebnissen, richtig angegeben. Das entspricht %.2f %%", correct, correctPercent);
  printf("\n===========================================================================\n");

  return 0;
}
