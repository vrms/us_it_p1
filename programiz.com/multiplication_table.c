#include <stdio.h>
#include <stdlib.h>

int main()

// The program below takes an integer input from the user and generates the multiplication tables up to 10.

{
  system("clear");

  printf("\n----------- up to 10 --------------------------\n");


  int i, n;

  printf("\n Please enter an integer: ");
  scanf("%d", &n);

  for(i=1; i<=10; i++)
  {
    printf("\n %2d * %2d = %3d ", n, i, n*i);
  }

  printf("\n------------ up to operand --------------------\n");

  int o;

  printf("\n Please enter an integer: ");
  scanf("%d", &n);
  printf("\n Please enter an operand: ");
  scanf("%d", &o);

  for(i=1; i<=o; i++)
  {
    printf("\n %2d * %2d = %3d ", n, i, n*i);
  }
    printf("\n");
   return 0;
}
