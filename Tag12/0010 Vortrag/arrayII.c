#include <stdio.h>
#include <stdlib.h>

int main()

{
  system("clear");

//int umsatz[z][s]
  int umsatz[3][4] =  {
                      {120, 210, 195, 172},   // z0
                      {100, 160, 140, 190},   // z1
                      {110, 312, 270, 210}    // z2
                      };

  printf("\n----------------------------------------------\n");

  // Ausgabe eines einzelnen Wertes
  printf("\n Der Umsatz Zeile 2 (index1), Spalte 3 (index2) ist: %d $", umsatz[1][2]);
  printf("\n");
  printf("\n----------------------------------------------\n");
  // Aendern eines Wertes
  // Setzen Sie den Umsatz der Filiale 2 im Quartal 4 auf 777
  // Z = Filiale, S = Quartal

  umsatz[1][3] = 777;
  printf("\n Der neue Umsatz Quartal 4 (index3), Filiale 2 (index1) ist: %d $", umsatz[1][3]);
  printf("\n----------------------------------------------\n");

  // Lassen sie Dass Array "umsatz" komplett ausgeben.
  // Dabei sollen je Filiale eine Zeile ausgegeben werden und die Werte der Filllialen nebeneinander stehen
  printf("\n------ Zeilenweise -------------------------\n");
  printf("\n              Q1      Q2      Q3      Q4");
  for(int z=0; z<3; z++)      //auessere Schleife fuer Zeilen
  {
    printf("\n Filiale %d:", z+1);
    for (int s=0; s<4; s++)   // innere Schleife fuer Spalten
    {
      printf("%6d |",umsatz[z][s] );
    }
  }


  // Jetzt bitte Spaltenweise ausgeben
  printf("\n------ Spaltenweise -------------------------\n");
  printf("\n              F1      F2      F3");
  for(int s=0; s<4; s++)      //auessere Schleife fuer Spalten
  {
    printf("\n Quartal: %d", s+1);
    for (int z=0; z<3; z++)   // innere Schleife fuer Zeilen
    {
      printf("%6d |",umsatz[z][s] );
    }
  }


  printf("\n");

   return 0;
}
