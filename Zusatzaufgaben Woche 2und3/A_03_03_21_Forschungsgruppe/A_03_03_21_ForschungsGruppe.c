/*

*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	system("chcp 1252");
	system("cls");
	
	int anzahl_der_daten, i;
	
	printf("\nAnzahl der Daten eingeben: ");
	scanf("%d", &anzahl_der_daten);
	
	// Deklarieren des arrays
	float array[anzahl_der_daten];
	
	// Eingabe der Daten
	i=0;
	while(i<anzahl_der_daten)
	{
		printf("\nWert %d eingeben: ", i+1);
		scanf("%f", &array[i]);
		i++;
	}
	
	printf("\n\n");
	
	i=anzahl_der_daten-1;
	while(i>=0)
	{
		//printf("\nWert %d ist %.2f ", i+1, array[i]);
		printf("Wert %d ist %.2f | ", i+1, array[i]);
		i--;
	}
	
	printf("\n\n");
	return 0;
}


