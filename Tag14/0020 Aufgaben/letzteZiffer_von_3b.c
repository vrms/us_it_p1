#include <stdio.h>
#include <stdlib.h>

// Sie sollen ein Programm entwickeln, das drei Ganzzahlen als Eingabe erhält und
// prüft, ob alle drei Zahlen unterschiedliche letzte Ziffern besitzen.
// Geben Sie das  Ergebnis auf der Konsole aus.
//
// Erstellen Sie zuerst den Pseudocode und entwickeln Sie anschließend das C Programm

// Modulo % koennte hilfreich sein

int main()



{
  system("clear");

  int num1, num2, num3;
  int cif1, cif2, cif3;

  printf("\nBitte 3 Ganzzahlen eingeben: \n");
  scanf("%d", &num1);
  scanf("%d", &num2);
  scanf("%d", &num3);

  cif1 = num1%10;
  cif2 = num2%10;
  cif3 = num3%10;

  if(cif1!=cif2 || cif1!=cif3)
  {
    printf("\n Die drei Zahlen (%d, %d, %d) nicht all die gleiche Ziffer am Ende", num1, num2, num3);
  }
  else
  {
    printf("\n Die drei Zahlen (%d, %d, %d) haben die gleiche finale ziffer %d", num1, num2, num3, cif1);
  }


  printf("\n----------------------------------------------\n");


   return 0;
}
